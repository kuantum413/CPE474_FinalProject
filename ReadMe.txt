Author: Keenan M. Reimer
Class: CPE 474
Professor: Shinjiro Sueda
Assignment: Final Project
Video: https://www.youtube.com/watch?v=UaYUSsyWuXI

Special note: If you get an error about texture2D or texture in fading_tex_phong_frag.glsl just switch from one to the other. It's an openGL version issue.

Key commands:
w & s: move forward or backward
a & d: strafe left or right
space: move up
**************
f: toggle missle-firing (disabled by default)
**************
g: re-generate the world
up-arrow: add a shark randomly in the world
page-up: increase the shark missle firing rate
page-down: decrease the shark missle firing rate

Mouse commands:
Left-Click: Drag to rotate camera

Features for CPE474:
- Skinned shark animation
- Physically based missle motion
- Explosion particle effect
- Missle thrust particle effect
- Waving seaweed animation

Features from CPE471:
- Blinn-Phong Lighting
- Textured objects
- Distance alpha-fading
- Proceedurally generated world with lots of meshes
- Floating bubbles animation
