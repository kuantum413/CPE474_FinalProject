#pragma once
#ifndef _PARTICLE_H_
#define _PARTICLE_H_

#include <memory>
#include <vector>
#include <list>
#include <cstdlib>
#include <cmath>
#include <limits>

#define GLEW_STATIC
#include <GL/glew.h>

//#define EIGEN_DONT_ALIGN_STATICALLY
#include <Eigen/Dense>

#include "Point.h"

double randRange(double l, double h);

class MatrixStack;
class Program;
class Texture;

class Particle
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	Particle();
	virtual ~Particle();

	// OpenGL methods
	static void init();
	static void bind(std::shared_ptr<Program> prog);
	void draw(std::shared_ptr<Program> prog, std::shared_ptr<MatrixStack> M) ;
	static void unbind(std::shared_ptr<Program> prog);

	// Misc. methods.
	bool isAlive(double t);
	
	// Getters
	float getRadius() const { return r; }
	float getAngle() const { return theta; }
	float getAngularVelocity() const { return w; }
	float getRadialVelocity() const { return rV; }
	Eigen::Vector3d getVelocity() const { return v; }
	Eigen::Vector3d getPosition() const { return x; }
	
	// Setters
	void setRadius(const float radius) { r = radius; }
	void setAngle(const float angle) { theta = angle; }
	void setAngularVelocity(const float angVel) { w = angVel; }
	void setVelocity(const Eigen::Vector3d vel) { v = vel; }
	void setPosition(const Eigen::Vector3d pos) { x = pos; }
	void setRadialVelocity(const float radVel) { rV = radVel; };
	void setBirth(const double t) { t0 = t; }
	void setLife(const double t) { life = t; }
	
private:
	// For physics
	Eigen::Vector3d x; // position
	Eigen::Vector3d v; // velocity
	Eigen::Vector3d a; // aceleration (force/mass)
	float theta;
	float w;  // Angular velocity.
	float rV;  // Radial velocity.
	
	// For display only
	float r;
	double t0;
	double life;
	static std::vector<float> posBuf;
	static std::vector<float> texBuf;
	static std::vector<unsigned int> indBuf;
	static GLuint posBufID;
	static GLuint texBufID;
	static GLuint indBufID;
};

#endif
