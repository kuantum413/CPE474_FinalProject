#include "Shark.h"
#include "Missle.h"
#include <iostream>

Shark::Shark() {
}

// Initialize a shark with meaningful random values
Shark::Shark(shared_ptr<SkinnedShape> sharkShape) {
	height = 1.0f;
	speed = 1.0f;
	radius = 1.0f;
	center << 0.0f, 0.0f, 0.0f;
	angularSpeed = speed / radius;
	thetaOffset = randRangef(0.0f, 2.0f * (float)M_PI);
	phiOffset = randRangef(0.0f, 2.0f * (float)M_PI);

	shape = sharkShape;
	M = make_shared<MatrixStack>();
}

Shark::~Shark() {
}

void Shark::setHeight(float h) {
	height = h;
}

void Shark::setRadius(float r) {
	radius = r;
	angularSpeed = speed / radius;
}

void Shark::setSpeed(float s) {
	speed = s;
	angularSpeed = speed / radius;
}

void Shark::setCenter(Vector3f cen) {
  center = cen;
}

// Fire a missle at the sharks location
shared_ptr<Missle> Shark::fireMissle(double t) {
	// Figure out the shark's position and radius
	Vector3f pos = E.block<3, 1>(0, 3);
	Vector3f r = pos - center;
	// From that calculate the missle's velocity
	Vector3f vel = speed * Vector3f(0.0f, 1.0f, 0.0f).cross(r).normalized();

	// Create the missle and set it's parameters
	shared_ptr<Missle> missle = make_shared<Missle>();
	missle->setInitialPosition(pos + 0.5f * vel.normalized());  // Move the missle forward a bit
	missle->setInitialVelocity(vel);
	missle->setInitialTime(t);

	return missle;
}

void Shark::prep(const shared_ptr<Program> prog, double t) {
	M->pushMatrix();

	// Place the Shark in the world.
	float theta = (float)t * angularSpeed + thetaOffset;
	M->translate(Vector3f(center(0), height, center(2)));  // Move to offset circle.
	M->rotate(theta, Vector3f(0.0f, 1.0f, 0.0f));  // Rotate about swim circle.
	M->translate(Vector3f(radius, 0.0f, 0.0f));  // Move out to the radius.
	M->rotate(3.0f * (float)M_PI / 180 * sin(speed * (float)t + phiOffset), Vector3f(0.0f, 1.0f, 0.0f));  // Rotate tangent to swim circle.
	if (speed > 0.0f) {
		M->rotate(-(float)M_PI, Vector3f(0.0f, 1.0f, 0.0f));  // Flip around if swimming clockwise.
	}

	// Send transform to GPU and configure animation
	glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, M->topMatrix().data());
	shape->prepGPU(prog, 0, abs(speed) * (float)t / 2);

	E = M->topMatrix();
	M->popMatrix();
}

void Shark::draw(const shared_ptr<Program> prog){
	shape->drawGPU(prog);
}

void Shark::setBones(shared_ptr<SkinnedShape> sharkShape) {
	vector<Matrix4f> bindBuf;

	// Head
	Matrix4f b0 = Matrix4f::Identity();
	b0.block<3, 1>(0, 3) = Vector3f(0.0f, 0.0f, -1.8f);
	bindBuf.push_back(b0.inverse());

	//// Body Front
	//Matrix4f b1 = Matrix4f::Identity();
	//b1.block<3, 1>(0, 3) = Vector3f(0.0f, 0.0f, -0.776f);
	//bindBuf.push_back(b1.inverse());

	// Body Center
	Matrix4f b2 = Matrix4f::Identity();
	b2.block<3, 1>(0, 3) = Vector3f(0.0f, 0.0f, 0.0f);
	bindBuf.push_back(b2.inverse());

	// Body back
	Matrix4f b3 = Matrix4f::Identity();
	b3.block<3, 1>(0, 3) = Vector3f(0.0f, 0.0f, 1.0f);
	bindBuf.push_back(b3.inverse());

	// Tail front
	Matrix4f b4 = Matrix4f::Identity();
	b4.block<3, 1>(0, 3) = Vector3f(0.0f, 0.0f, 2.25f);
	bindBuf.push_back(b4.inverse());

	// Tail back
	Matrix4f b5 = Matrix4f::Identity();
	b5.block<3, 1>(0, 3) = Vector3f(0.0f, 0.0f, 3.578f);
	bindBuf.push_back(b5.inverse());

	sharkShape->setBindPose(bindBuf);
}

void Shark::setAnimation(shared_ptr<SkinnedShape> sharkShape) {
	vector<Matrix4f> poseBuf;

	// Parameters to determine how much the shark 'wiggles' when it swims.
	int frames = 250;
	float dTheta = 2.0f * (float)M_PI / frames;
	float phiExp = 1.1f;
	float phi = 5.0f * (float)M_PI / 180;

	shared_ptr<MatrixStack> M = make_shared<MatrixStack>();
	for (int i = 0; i < frames; i++) {
		float theta = i * dTheta;

		// Add a body wide rotation so the center is less static.
		M->pushMatrix();
		M->rotate(5.0f * (float) M_PI / 180 * sin(theta), Vector3f(0.0f, 1.0f, 0.0f));

		M->pushMatrix();
		// Head.
		M->rotate(phi * sin(theta), Vector3f(0.0f, 1.0f, 0.0f));
		M->pushMatrix();
		M->translate(Vector3f(0.0f, 0.0f, -1.8f));
		poseBuf.push_back(M->topMatrix());
		M->popMatrix();

		//// Body front
		//M->rotate(phi * sin(theta), Vector3f(0.0f, 1.0f, 0.0f));
		//M->pushMatrix();
		//M->translate(Vector3f(0.0f, 0.0f, -0.776f));
		//poseBuf.push_back(M->topMatrix());
		//M->popMatrix();
		
		M->popMatrix();
		// Body center
		poseBuf.push_back(M->topMatrix());
		M->pushMatrix();

		// Body back
		M->rotate(-1.0f * phi * sin(theta), Vector3f(0.0f, 1.0f, 0.0f));
		M->pushMatrix();
		M->translate(Vector3f(0.0f, 0.0f, 1.0f));
		poseBuf.push_back(M->topMatrix());
		M->popMatrix();

		// Tail Front
		M->rotate(-phi * sin(theta), Vector3f(0.0f, 1.0f, 0.0f));
		M->pushMatrix();
		M->translate(Vector3f(0.0f, 0.0f, 2.25f));
		poseBuf.push_back(M->topMatrix());
		M->popMatrix();

		// Tail back
		M->rotate(- 0.5f * phi * sin(theta), Vector3f(0.0f, 1.0f, 0.0f));
		M->pushMatrix();
		M->translate(Vector3f(0.0f, 0.0f, 3.578f));
		poseBuf.push_back(M->topMatrix());
		M->popMatrix();

		M->popMatrix();

		M->popMatrix();
	}
	sharkShape->addAnimation(poseBuf);
}
