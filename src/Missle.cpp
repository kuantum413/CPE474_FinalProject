#include "Missle.h"
#include <iostream>

Missle::Missle() {
	M = make_shared<MatrixStack>();
}

Missle::Missle(shared_ptr<Shape> missleShape) {
	shape = missleShape;
	M = make_shared<MatrixStack>();
}

Missle::~Missle() {
}

void Missle::setInitialPosition(Vector3f initPos) {
	p = initPos;
}

void Missle::setInitialVelocity(Vector3f initVel) {
	v = initVel;
}

void Missle::setInitialTime(double t) {
	t0 = (float)t;
}

void Missle::setShape(shared_ptr<Shape> newShape) {
	shape = newShape;
}

// Check to see if the missle has collided with the ground
bool Missle::checkHit() {
	if (p[1] <= 0.0) {
		return true;
	}
	return false;
}

// Prepare the missle's position & orientation
void Missle::prep(shared_ptr<Program> prog, const double t) {
	M->pushMatrix();

	// Calculate missle's acceleration based on gravity and thrust
	Vector3f a = Vector3f(0.0f, -1.0f, 0.0f) + 20.0f * v.normalized();

	// Update velocity and position
	v += (float(t) - t0) * a;
	prevP = p;
	p += (float(t) - t0) * v;

	// Calculate useful orientation parameters
	Vector3f vNorm = v.normalized();
	Vector2f vXZ(vNorm[0], vNorm[2]);
	Vector2f vXZNorm = vXZ.normalized();

	M->translate(p);  // Move the missle to its position
	// Orient the missle about the y-axis
	if (vXZNorm[0] > 0.0f) {
		M->rotate(-asin(vXZNorm[1]), Vector3f(0.0f, 1.0f, 0.0f));
	}
	else {  // Flipping it around as well of the x component of the velocity is negative
		M->rotate(asin(vXZNorm[1]) + (float) M_PI, Vector3f(0.0f, 1.0f, 0.0f));
	}
	// Orient the missle about the z-axis
	M->rotate(asin(vNorm[1]), Vector3f(0.0f, 0.0f, 1.0f));
	glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, M->topMatrix().data());

	t0 = float(t);
	M->popMatrix();
}

void Missle::draw(const shared_ptr<Program> prog){
	shape->draw(prog);
}