/* Lab 5 base code - transforms using local matrix functions 
   to be written by students - 
	CPE 471 Cal Poly Z. Wood + S. Sueda
*/
#include <iostream>
#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "GLSL.h"
#include "Program.h"
#include "Texture.h"
#include "Shark.h"
#include "Bubbles.h"
#include "Missle.h"
#include "Particle.h"
#include "Point.h"

using namespace std;
using namespace Eigen;

bool keyToggles[256] = { false }; // only for English keyboards!

GLFWwindow *window; // Main application window
string RESOURCE_DIR = ""; // Where the resources are loaded from

// All of the GPU programs.
// Progamatically defined material with phong lighting.
shared_ptr<Program> fadePhongProg;
// Textured diffuse lighting with programatically defined specular.
shared_ptr<Program> fadeTexPhongProg;
// Programatically defined material that waves with time with phong lighting. 
shared_ptr<Program> fadeWavePhongProg;
// Skinned mesh with programatically defined material and phong lighting.
shared_ptr<Program> fadeSkinPhongProg;
// Textured particle effect that fades with distance.
shared_ptr<Program> fadeParticleProg;

// All of the loaded OBJ's
shared_ptr<Shape> plane;
shared_ptr<Shape> nefertiti;
shared_ptr<Shape> rock0;
shared_ptr<Shape> rock1;
shared_ptr<Shape> rock2;
shared_ptr<Shape> rock3;
shared_ptr<Shape> seaweed;
shared_ptr<Shape> nautilus;
shared_ptr<Shape> submarine;
shared_ptr<Shape> missleShape;

// All of the loaded textures.
shared_ptr<Texture> sandTex;
shared_ptr<Texture> surfaceTex;
shared_ptr<Texture> coral0Tex;
shared_ptr<Texture> coral1Tex;
shared_ptr<Texture> coral2Tex;
shared_ptr<Texture> rustedMetalTex0;
shared_ptr<Texture> rustedMetalTex1;
shared_ptr<Texture> rockParticleTex;
shared_ptr<Texture> rockParticleAlphaTex;
shared_ptr<Texture> bubbleTex;
shared_ptr<Texture> bubbleAlphaTex;

// Higher order objects that handle their own motions.
shared_ptr<BubblesShapes> bubblesShapes;
vector<shared_ptr<Bubbles>> bubbles;
shared_ptr<SkinnedShape> sharkShape;
vector<shared_ptr<Shark>> sharks;
vector<shared_ptr<Missle>> missles;

// Transform objects that are handled by main.
vector<shared_ptr<Particle>> rockParticles;
vector<shared_ptr<Particle>> thrustParticles;
vector<vector<Matrix4f>> rockTransforms;
vector<Matrix4f> seaweedTransforms;
vector<Matrix4f> nautilusTransforms;
vector<Matrix4f> submarineTransforms;

// Camera variables.
float mouse[2];
float alpha, beta, alphaClamp;
Vector3f camPos;
Vector3f camDir;
float camSpeed;
float viewDist;

// Misc. variables.
int g_width, g_height;
float mpsps;  // Missles per second per shark.
double t0;  // t of previous frame.

			// Add a new shark randomly in the world
static void addAShark() {
	// Define ranges
	Vector2f sR(20.0f, viewDist);  // Range of swim radii
	Vector2f cen(-viewDist / 2, viewDist / 2);  // Range of swim center x, y's
	Vector2f sH(1.0f, 25.0f);  // Range of swim height
	Vector2f sS(-3.0f, 3.0f);  // Range of swim speed

	float s = randRangef(sS(0), sS(1));  // Random swim speed
	if (abs(s) < 1.0f) {  // Make sure it is at least 1
		s = s / abs(s);
	}
	float h = randRangef(sH(0), sH(1));  // Random swim height
	float r = randRangef(sR(0), sR(1));  // Random swim radius
	float cenX = randRangef(cen(0), cen(1));  // Random center x
	float cenY = randRangef(cen(0), cen(1));  // Random center y

											  // Create the shark and pass along variables
	shared_ptr<Shark> shark = make_shared<Shark>(sharkShape);
	shark->setCenter(Vector3f(cenX, 0.0f, cenY));
	shark->setHeight(h);
	shark->setRadius(r);
	shark->setSpeed(s);
	sharks.push_back(shark);  // Add it to the vector of sharks.
}

// Populate the world with semi-random objects.
static void generate() {
	// Clear out any old objects and particles.
	sharks.clear();
	bubbles.clear();
	missles.clear();
	rockTransforms.clear();
	seaweedTransforms.clear();
	nautilusTransforms.clear();
	submarineTransforms.clear();
	rockParticles.clear();

	// Initialize random sharks around Nefertiti.
	int numSharks = 40;
	Vector2f r(8.0f, 15.0f);
	Vector2f h(1.0f, 25.0f);
	Vector2f s(-3.0f, 3.0f);
	for (int i = 0; i < numSharks; i++) {
		float sS = randRangef(s(0), s(1));
		if (abs(sS) < 1.0f) {
			sS = sS / abs(sS);
		}
		float sH = randRangef(h(0), h(1));
		float sR = randRangef(r(0), r(1));

		shared_ptr<Shark> shark = make_shared<Shark>(sharkShape);
		shark->setHeight(sH);
		shark->setRadius(sR);
		shark->setSpeed(sS);
		sharks.push_back(shark);
	}

	// Initial random sharks elsewhere in the world.
	numSharks = 30;
	for (int i = 0; i < numSharks; i++) {
		addAShark();
	}

	// Initialize random bubbles.
	int numBubbles = 200;
	bubblesShapes = make_shared<BubblesShapes>(RESOURCE_DIR);
	for (int i = 0; i < numBubbles; i++) {
		shared_ptr<Bubbles> bubble = make_shared<Bubbles>(bubblesShapes);
		bubbles.push_back(bubble);
	}

	// Initialize random rock transforms.
	int numRock0 = 500;
	Vector2f rSC(3.0f, 10.0f);
	Vector2f rR(30.0f, 300.0f);
	for (int i = 0; i < 4; i++) {
		rockTransforms.push_back(vector<Matrix4f>());
		for (int j = 0; j < numRock0 / 4; j++) {
			auto E = make_shared<MatrixStack>();
			E->loadIdentity();
			float sc = randRangef(rSC(0), rSC(1));
			float r = randRangef(rR(0), rR(1));
			float rot = randRangef(0.0f, 2.0f * (float)M_PI);
			float theta = randRangef(0.0f, 2.0f * (float)M_PI);

			E->translate(Vector3f(r * cos(theta), 0.0f, r * sin(theta)));
			E->rotate(rot, Vector3f(0.0f, 1.0f, 0.0f));
			E->scale(sc);

			rockTransforms[i].push_back(E->topMatrix());
		}
	}

	// Initialize random seaweed transforms.
	int numSeaweed = 100;
	Vector2f swSC(5.0f, 10.0f);
	Vector2f swR(15.0f, 300.0f);
	Vector2f swDR(0.0f, 0.001f);
	Vector2f swDensity(3.0f, 10.0f);
	for (int i = 0; i < numSeaweed; i++) {
		float r = randRangef(swR(0), swR(1));
		float theta = randRangef(0.0f, 2.0f * (float)M_PI);

		int num = (int)randRangef(swDensity(0), swDensity(1));
		for (int j = 0; j < num; j++) {
			float sc = randRangef(swSC(0), swSC(1));
			float rot = randRangef(0.0f, 2.0f * (float)M_PI);
			float dR = randRangef(swDR(0), swDR(1));
			float dTheta = randRangef(0.0f, 2.0f * (float)M_PI);

			auto E = make_shared<MatrixStack>();
			E->loadIdentity();
			E->translate(Vector3f(r * cos(theta), 0.0f, r * sin(theta)));
			E->translate((Vector3f(dR * cos(dTheta), 0.0f, dR * sin(dTheta))));
			E->rotate(rot, Vector3f(0.0f, 1.0f, 0.0f));
			E->scale(sc);
			seaweedTransforms.push_back(E->topMatrix());
		}
	}

	// Initialize random nautilus transforms.
	int numNaut = 3;
	Vector2f nR(100.0f, 300.0f);
	Vector2f nXRot(-30.0f * (float)M_PI / 180, 30.0f * (float)M_PI / 180);
	for (int i = 0; i < numNaut; i++) {
		auto E = make_shared<MatrixStack>();
		E->loadIdentity();
		float r = randRangef(nR(0), nR(1));
		float xRot = randRangef(nXRot(0), nXRot(1));
		float yRot = randRangef(0.0f, 2.0f * (float)M_PI);
		float zRot = randRangef(0.0f, 2.0f * (float)M_PI);
		float theta = randRangef(0.0f, 2.0f * (float)M_PI);

		E->translate(Vector3f(r * cos(theta), 0.0f, r * sin(theta)));
		E->rotate(yRot, Vector3f(0.0f, 1.0f, 0.0f));
		E->rotate(xRot, Vector3f(1.0f, 0.0f, 0.0f));
		E->rotate(zRot, Vector3f(0.0f, 0.0f, 1.0f));
		E->scale(50.0f);

		nautilusTransforms.push_back(E->topMatrix());
	}

	// Initialize random submarine transforms.
	int numSub = 2;
	Vector2f subR(75.0f, 300.0f);
	Vector2f subZRot(-30.0f * (float)M_PI / 180, 30.0f * (float)M_PI / 180);
	for (int i = 0; i < numSub; i++) {
		auto E = make_shared<MatrixStack>();
		E->loadIdentity();
		float r = randRangef(subR(0), subR(1));
		float xRot = randRangef(0.0f, 2.0f * (float)M_PI);
		float yRot = randRangef(0.0f, 2.0f * (float)M_PI);
		float zRot = randRangef(subZRot(0), subZRot(1));
		float theta = randRangef(0.0f, 2.0f * (float)M_PI);

		E->translate(Vector3f(r * cos(theta), 0.0f, r * sin(theta)));
		E->rotate(yRot, Vector3f(0.0f, 1.0f, 0.0f));
		E->rotate(zRot, Vector3f(0.0f, 0.0f, 1.0f));
		E->rotate(xRot, Vector3f(1.0f, 0.0f, 0.0f));
		E->scale(25.0f);

		submarineTransforms.push_back(E->topMatrix());
	}
}

// Destroy anything within a certain radius of an explosion.
static void explosion(Vector3f center, float dist) {
	// Check to destroy rocks.
	for (int i = 0; i < (int)rockTransforms.size(); i++) {
		for (int j = 0; j < (int)rockTransforms[i].size(); j++) {
			Matrix4f E = rockTransforms[i][j];
			Vector3f pos = E.block<3, 1>(0, 3);
			Vector3f vecDist = pos - center;
			if (vecDist.norm() < dist) {
				rockTransforms[i].erase(rockTransforms[i].begin() + j);
			}
		}
	}

	// Check to destroy seaweeds.
	for (int i = 0; i < (int)seaweedTransforms.size(); i++) {
		Matrix4f E = seaweedTransforms[i];
		Vector3f pos = E.block<3, 1>(0, 3);
		Vector3f vecDist = pos - center;
		if (vecDist.norm() < dist) {
			seaweedTransforms.erase(seaweedTransforms.begin() + i);
		}
	}

	// Generate particles at explosion position.
	int numParticles = 30;
	double t = glfwGetTime();
	numParticles = int((float)numParticles * (500.0f - (float)rockParticles.size()) / 500.0f);
	numParticles = max(numParticles, 0);
	for (int i = 0; i < numParticles; i++) {
		shared_ptr<Particle> particle = make_shared<Particle>();

		double beta = randRange(0.0, 2.0 * M_PI);
		double alpha = randRange(0.0, M_PI);
		double r = randRange(0.0, 1.0);

		double x = (double)center[0] + r * (cos(alpha) * cos(beta));
		double y = r * sin(alpha);
		double z = (double)center[2] + r * (cos(alpha) * cos(M_PI / 2 - beta));
		Vector3d position(x, y, z);

		Vector3d velocity = 20.0 * (r / 1.0f) * (Vector3d(x, y, z) - center.cast<double>()).normalized();

		particle->setRadius(randRangef(0.4f, 1.2f));
		particle->setAngularVelocity(randRangef(-8.0f * (float)M_PI, 8.0f * (float)M_PI));
		particle->setPosition(position);
		particle->setVelocity(velocity);
		rockParticles.push_back(particle);
	}
}

void thrust(Vector3f pos1, Vector3f pos0) {
	// Set initial parameters.
	int numParticles = 2;
	float focalLength = 1.0f;
	double t = glfwGetTime();

	// Calculate useful geometric tools.
	Vector3f dist = pos1 - pos0;
	float distNorm = dist.norm();
	Vector3f vel = dist / float(t - t0);
	Vector3f velDir = vel.normalized();
	float speed = vel.norm();
	Vector4f perpVec(-velDir[1], velDir[0], velDir[2], 1.0);  // A unit length vector perp to velDir.

	// Back the positions off to the back of the torpedo.
	pos0 += -2.0f * velDir;
	pos1 += -2.0f * velDir;

	Matrix4f E = Matrix4f::Identity();
	numParticles = int((float) numParticles * (1000.0f - (float)thrustParticles.size()) / 1000.0f);
	numParticles = max(numParticles, 0);
	for (int i = 0; i < numParticles; i++) {
		shared_ptr<Particle> particle = make_shared<Particle>();

		float d = randRangef(0.0, distNorm);
		float alpha = randRangef(0.0, 2.0f * (float)M_PI);
		float r = randRangef(0.0f, 0.3f);
		E.block<3, 3>(0, 0) = AngleAxisf(alpha, velDir).toRotationMatrix();

		Vector4f perpD = r * E * perpVec;
		Vector3f position = pos0 + d * velDir + perpD.segment<3>(0);

		// Find the focal length relative to the time at which this particle was created.
		float tp = d / speed;
		Vector3f focalPos = pos0 + tp * vel + focalLength * velDir;
		Vector3f velocity = 1.0 * (position - focalPos).normalized();

		particle->setBirth(t);
		particle->setLife(1.5);
		particle->setRadius(randRangef(0.05f, 0.15f));
		particle->setPosition(position.cast<double>());
		particle->setVelocity(velocity.cast<double>());
		thrustParticles.push_back(particle);
	}

}

static void error_callback(int error, const char *description)
{
	cerr << description << endl;
}

static void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, GL_TRUE);
	}

	// Move the camera around.
	else if (key == GLFW_KEY_W) {
	  camPos += camSpeed * camDir;
	}
	else if (key == GLFW_KEY_A) {
	  Vector3f camCross = camDir.cross(Vector3f(0.0f, 1.0f, 0.0f)).normalized();
	  camPos -= camCross;
	}
	else if (key == GLFW_KEY_S) {
	  camPos -= camSpeed * camDir;
	}
	else if (key == GLFW_KEY_D) {
	  Vector3f camCross = camDir.cross(Vector3f(0.0f, 1.0f, 0.0f)).normalized();
	  camPos += camCross;
	}
	else if (key == GLFW_KEY_SPACE) {
	  camPos += camSpeed * Vector3f(0.0f, 1.0f, 0.0f);
	}
	// Re-generate world objects.
	else if (key == GLFW_KEY_G && action == GLFW_PRESS) {
		generate();
	}
	// Add a shark.
	else if (key == GLFW_KEY_UP) {
		addAShark();
	}
	// Change missle firing rate
	else if (key == GLFW_KEY_PAGE_UP) {
		mpsps += 0.1f;
	}
	else if (key == GLFW_KEY_PAGE_DOWN) {
		mpsps += -0.1f;
	}
}

static void char_callback(GLFWwindow *window, unsigned int key)
{
	keyToggles[key] = !keyToggles[key];
}


static void mouse_callback(GLFWwindow *window, int button, int action, int mods)
{
   double posX, posY;
   if (action == GLFW_PRESS) {
     glfwGetCursorPos(window, &posX, &posY);
     mouse[0] = (float) posX;
     mouse[1] = (float) posY;
   }
}

static void cursor_pos_callback(GLFWwindow * window, double xpos, double ypos) {
	int mouseAction = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
	if (mouseAction == GLFW_PRESS) {
		double posX, posY;
		glfwGetCursorPos(window, &posX, &posY);
		float dX = (float) posX - mouse[0];
		float dY = (float) posY - mouse[1];

		int width, height;
		glfwGetFramebufferSize(window, &width, &height);

		alpha -= 0.5f * (float) M_PI * dY / width;
		beta += (float) M_PI * dX / height;

		// Clamp alpha between +/- 85 degrees.
		if (alpha > alphaClamp) {
		  alpha = alphaClamp;
		}
		else if (alpha < -alphaClamp) {
		  alpha = -alphaClamp;
		}

		// Change camera angle.
		camDir(0) = cos(alpha) * cos(beta);
		camDir(1) = sin(alpha);
		camDir(2) = cos(alpha) * cos(0.5f * (float) M_PI - beta);

		// Record previous mouse position.
		mouse[0] = (float) posX;
		mouse[1] = (float) posY;
	}
}

static void resize_callback(GLFWwindow *window, int width, int height) {
   g_width = width;
   g_height = height;
   glViewport(0, 0, width, height);
}

// Set material parameters to GLSL program.
void setMaterial(int i, shared_ptr<Program> prog) {

  switch (i) {
  case 0: //shiny red metal
    glUniform3f(prog->getUniform("matAmb"), 0.004f, 0.002f, 0.02f);
    glUniform3f(prog->getUniform("matDif"), 0.9f, 0.16f, 0.0f);
    glUniform3f(prog->getUniform("matSpec"), 0.7f, 0.6f, 0.6f);
    glUniform1f(prog->getUniform("matShine"), 27.9f);
    break;
  case 1: // shiny grey
    glUniform3f(prog->getUniform("matAmb"), 0.013f, 0.013f, 0.014f);
    glUniform3f(prog->getUniform("matDif"), 0.3f, 0.3f, 0.4f);
    glUniform3f(prog->getUniform("matSpec"), 0.3f, 0.3f, 0.4f);
    glUniform1f(prog->getUniform("matShine"), 27.9f);
    break;
  case 2: // brass
      glUniform3f(prog->getUniform("matAmb"), 2.0f * 0.03294f, 2.0f * 0.02235f, 2.0f * 0.002745f);
      glUniform3f(prog->getUniform("matDif"), 0.7804f, 0.5686f, 0.11373f);
      glUniform3f(prog->getUniform("matSpec"), 0.9922f, 0.941176f, 0.80784f);
      glUniform1f(prog->getUniform("matShine"), 27.9f);
      break;
  case 3: // copper
    glUniform3f(prog->getUniform("matAmb"), 0.01913f, 0.00735f, 0.00225f);
    glUniform3f(prog->getUniform("matDif"), 0.7038f, 0.27048f, 0.0828f);
    glUniform3f(prog->getUniform("matSpec"), 0.257f, 0.1376f, 0.08601f);
    glUniform1f(prog->getUniform("matShine"), 12.8f);
    break;
  case 4: // aquamarine
    glUniform3f(prog->getUniform("matAmb"), 0.0f, 0.026f, 0.013f);
    glUniform3f(prog->getUniform("matDif"), 0.0f, 0.8f, 0.7f);
    glUniform3f(prog->getUniform("matSpec"), 0.0f, 0.2f, 0.99f);
    glUniform1f(prog->getUniform("matShine"), 20.0f);
    break;
  case 5: // shark
	  glUniform3f(prog->getUniform("matAmb"), 0.25098f / 3, 0.192157f / 3, 0.282353f / 3);
	  glUniform3f(prog->getUniform("matDif"), 0.25098f, 0.192157f, 0.282353f);
	  glUniform3f(prog->getUniform("matSpec"), 0.3f, 0.3f, 0.3f);
	  glUniform1f(prog->getUniform("matShine"), 5.0f);
	  break;
  case 6: // bubbles
	  glUniform3f(prog->getUniform("matAmb"), 0.9f, 0.9f, 0.9f);
	  glUniform3f(prog->getUniform("matDif"), 0.0f, 0.0f, 0.0f);
	  glUniform3f(prog->getUniform("matSpec"), 0.1f, 0.1f, 0.1f);
	  glUniform1f(prog->getUniform("matShine"), 20.0f);
	  break;
  case 7: // seaweed
	  glUniform3f(prog->getUniform("matAmb"), 0.0f, 0.2f, 0.1f);
	  glUniform3f(prog->getUniform("matDif"), 0.0f, 0.4f, 0.0f);
	  glUniform3f(prog->getUniform("matSpec"), 0.4f, 0.5f, 0.4f);
	  glUniform1f(prog->getUniform("matShine"), 20.0);
	  break;
  }
}

// Set material parameters to a textured GLSL program.
void setTextureMaterial(int i, shared_ptr<Program> prog) {

	switch (i) {
	case 0: // sand
		glUniform1f(prog->getUniform("matAmb"), 0.25f);
		glUniform1f(prog->getUniform("matDif"), 1.0f);
		glUniform3f(prog->getUniform("matSpec"), 0.3f, 0.3f, 0.3f);
		glUniform1f(prog->getUniform("matShine"), 5.0f);
		break;
	case 1: // water surface
		glUniform1f(prog->getUniform("matAmb"), 0.25f);
		glUniform1f(prog->getUniform("matDif"), 1.0f);
		glUniform3f(prog->getUniform("matSpec"), 2.0f, 2.0f, 2.0f);
		glUniform1f(prog->getUniform("matShine"), 10.0f);
		break;
	case 2: // coral
		glUniform1f(prog->getUniform("matAmb"), 0.25f);
		glUniform1f(prog->getUniform("matDif"), 1.0f);
		glUniform3f(prog->getUniform("matSpec"), 0.3f, 0.3f, 0.3f);
		glUniform1f(prog->getUniform("matShine"), 5.0f);
		break;
	case 3: // nautilus
		glUniform1f(prog->getUniform("matAmb"), 0.25f);
		glUniform1f(prog->getUniform("matDif"), 1.0f);
		glUniform3f(prog->getUniform("matSpec"), 0.3f, 0.3f, 0.3f);
		glUniform1f(prog->getUniform("matShine"), 45.0f);
		break;
	case 4: // submarine
		glUniform1f(prog->getUniform("matAmb"), 0.25f);
		glUniform1f(prog->getUniform("matDif"), 1.0f);
		glUniform3f(prog->getUniform("matSpec"), 0.0f, 0.45f, 0.45f);
		glUniform1f(prog->getUniform("matShine"), 45.0f);
		break;
	}
}


static void init()
{
	GLSL::checkVersion();

	// Initialize camera variables.
	viewDist = 300.0f;
	camSpeed = 0.25f;
	alpha = 0.0f;
	alphaClamp = 85.0f * (float)M_PI / 180;
	beta = 0.5f * (float) M_PI;
	camPos = Vector3f(0.0f, 1.0f, -15.0f);
	camDir = Vector3f(0.0f, 0.0f, 1.0f);

	// Initialize world variables.
	mpsps = 0.01f;
	t0 = glfwGetTime();

	// Set background color.
	glClearColor(.051f, .553f, .875f, 1.0f);
	// Enable z-buffer test.
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	// Initialize skinned shark shape.
	sharkShape = make_shared<SkinnedShape>();
	sharkShape->loadMesh(RESOURCE_DIR + "Shark.obj");
	Shark::setBones(sharkShape);
	Shark::setAnimation(sharkShape);
	sharkShape->computeWeightsByGeometry(2.0f, -2.0f);
	sharkShape->init();

	// Initialize sand.
	plane = make_shared<Shape>();
	plane->loadMesh(RESOURCE_DIR + "plane.obj");
	plane->resize();
	plane->init();

	// Initialize mesh.
	nefertiti = make_shared<Shape>();
	nefertiti->loadMesh(RESOURCE_DIR + "Nefertiti-10K.obj");
	nefertiti->resize();
	nefertiti->init();

	// Initialize rock 0.
	rock0 = make_shared<Shape>();
	rock0->loadMesh(RESOURCE_DIR + "Boulder1.obj");
	rock0->resize();
	rock0->init();

	// Initialize rock 1.
	rock1 = make_shared<Shape>();
	rock1->loadMesh(RESOURCE_DIR + "Boulder2.obj");
	rock1->resize();
	rock1->init();

	// Initialize rock 2.
	rock2 = make_shared<Shape>();
	rock2->loadMesh(RESOURCE_DIR + "Boulder3.obj");
	rock2->resize();
	rock2->init();

	// Initialize rock 3.
	rock3 = make_shared<Shape>();
	rock3->loadMesh(RESOURCE_DIR + "rockArc.obj");
	rock3->resize();
	rock3->moveToGround();
	rock3->init();

	// Initialize seaweed.
	seaweed = make_shared<Shape>();
	seaweed->loadMesh(RESOURCE_DIR + "seaweed.obj");
	seaweed->resize();
	seaweed->moveToGround();
	seaweed->init();

	// Initialize nautilus.
	nautilus = make_shared<Shape>();
	nautilus->loadMesh(RESOURCE_DIR + "nautilus.obj");
	nautilus->resize();
	nautilus->init();

	// Initialize submarine.
	submarine = make_shared<Shape>();
	submarine->loadMesh(RESOURCE_DIR + "submarine.obj");
	submarine->resize();
	submarine->init();

	// Initialize missle.
	missleShape = make_shared<Shape>();
	missleShape->loadMesh(RESOURCE_DIR + "missle.obj");
	missleShape->init();

	// Initialize particles.
	Particle::init();

	// Initialize sand texture.
	sandTex = make_shared<Texture>();
	sandTex->setFilename(RESOURCE_DIR + "sandLight.png");
	sandTex->init();

	// Initialize ocean surface texture.
	surfaceTex = make_shared<Texture>();
	surfaceTex->setFilename(RESOURCE_DIR + "oceanSurface.png");
	surfaceTex->init();

	// Initialize coral texture 0.
	coral0Tex = make_shared<Texture>();
	coral0Tex->setFilename(RESOURCE_DIR + "coral0.png");
	coral0Tex->init();

	// Initialize coral texture 1.
	coral1Tex = make_shared<Texture>();
	coral1Tex->setFilename(RESOURCE_DIR + "coral1.png");
	coral1Tex->init();

	// Initialize coral texture 2.
	coral2Tex = make_shared<Texture>();
	coral2Tex->setFilename(RESOURCE_DIR + "coral2.png");
	coral2Tex->init();

	// Initialize first rust texture.
	rustedMetalTex0 = make_shared<Texture>();
	rustedMetalTex0->setFilename(RESOURCE_DIR + "rustedMetal0.png");
	rustedMetalTex0->init();

	// Initialize second rust texture.
	rustedMetalTex1 = make_shared<Texture>();
	rustedMetalTex1->setFilename(RESOURCE_DIR + "rustedMetal1.png");
	rustedMetalTex1->init();

	// Initialize the rock particle texture.
	rockParticleTex = make_shared<Texture>();
	rockParticleTex->setFilename(RESOURCE_DIR + "rockParticle.png");
	rockParticleTex->init();

	// Initialize the rock particle texture alpha.
	rockParticleAlphaTex = make_shared<Texture>();
	rockParticleAlphaTex->setFilename(RESOURCE_DIR + "rockParticleAlpha.png");
	rockParticleAlphaTex->init();

	// Initialize the bubble texture.
	bubbleTex = make_shared<Texture>();
	bubbleTex->setFilename(RESOURCE_DIR + "bubble.png");
	bubbleTex->init();

	// Initialize the bubble texture alpha.
	bubbleAlphaTex = make_shared<Texture>();
	bubbleAlphaTex->setFilename(RESOURCE_DIR + "bubbleAlpha.png");
	bubbleAlphaTex->init();

	// Populate the world initially.
	generate();

	// Initialize the fading phong program.
	fadePhongProg = make_shared<Program>();
	fadePhongProg->setVerbose(true);
	fadePhongProg->setShaderNames(RESOURCE_DIR + "fading_phong_vert.glsl", RESOURCE_DIR + "fading_phong_frag.glsl");
	fadePhongProg->init();
	fadePhongProg->addUniform("P");
	fadePhongProg->addUniform("V");
	fadePhongProg->addUniform("M");
	fadePhongProg->addUniform("camPos");
	fadePhongProg->addUniform("lightPos");
	fadePhongProg->addUniform("lightCol");
	fadePhongProg->addUniform("viewDist");
	fadePhongProg->addUniform("matAmb");
	fadePhongProg->addUniform("matDif");
	fadePhongProg->addUniform("matSpec");
	fadePhongProg->addUniform("matShine");
	fadePhongProg->addUniform("baseAlpha");
	fadePhongProg->addAttribute("vertPos");
	fadePhongProg->addAttribute("vertNor");

	// Initialize the fading waving phong program
	fadeWavePhongProg = make_shared<Program>();
	fadeWavePhongProg->setVerbose(true);
	fadeWavePhongProg->setShaderNames(RESOURCE_DIR + "fading_waving_phong_vert.glsl", RESOURCE_DIR + "fading_waving_phong_frag.glsl");
	fadeWavePhongProg->init();
	fadeWavePhongProg->addUniform("P");
	fadeWavePhongProg->addUniform("V");
	fadeWavePhongProg->addUniform("M");
	fadeWavePhongProg->addUniform("t");
	fadeWavePhongProg->addUniform("wave");
	fadeWavePhongProg->addUniform("camPos");
	fadeWavePhongProg->addUniform("lightPos");
	fadeWavePhongProg->addUniform("lightCol");
	fadeWavePhongProg->addUniform("viewDist");
	fadeWavePhongProg->addUniform("matAmb");
	fadeWavePhongProg->addUniform("matDif");
	fadeWavePhongProg->addUniform("matSpec");
	fadeWavePhongProg->addUniform("matShine");
	fadeWavePhongProg->addAttribute("vertPos");
	fadeWavePhongProg->addAttribute("vertNor");

	// Initalize the fading textured phong program
	fadeTexPhongProg = make_shared<Program>();
	fadeTexPhongProg->setVerbose(true);
	fadeTexPhongProg->setShaderNames(RESOURCE_DIR + "fading_tex_phong_vert.glsl", RESOURCE_DIR + "fading_tex_phong_frag.glsl");
	fadeTexPhongProg->init();
	fadeTexPhongProg->addUniform("P");
	fadeTexPhongProg->addUniform("V");
	fadeTexPhongProg->addUniform("M");
	fadeTexPhongProg->addUniform("camPos");
	fadeTexPhongProg->addUniform("lightPos");
	fadeTexPhongProg->addUniform("lightCol");
	fadeTexPhongProg->addUniform("viewDist");
	fadeTexPhongProg->addUniform("matAmb");
	fadeTexPhongProg->addUniform("matDif");
	fadeTexPhongProg->addUniform("matSpec");
	fadeTexPhongProg->addUniform("matShine");
	fadeTexPhongProg->addUniform("texture0");
	fadeTexPhongProg->addAttribute("vertPos");
	fadeTexPhongProg->addAttribute("vertNor");
	fadeTexPhongProg->addAttribute("vertTex");

	// Initialize the fading skinned phong program
	fadeSkinPhongProg = make_shared<Program>();
	fadeSkinPhongProg->setShaderNames(RESOURCE_DIR + "fading_skin_phong_vert.glsl", RESOURCE_DIR + "fading_skin_phong_frag.glsl");
	fadeSkinPhongProg->setVerbose(true); // Set this to true when debugging.
	fadeSkinPhongProg->init();
	fadeSkinPhongProg->addUniform("P");
	fadeSkinPhongProg->addUniform("V");
	fadeSkinPhongProg->addUniform("M");
	fadeSkinPhongProg->addUniform("M0");
	fadeSkinPhongProg->addUniform("M1");
	fadeSkinPhongProg->addUniform("camPos");
	fadeSkinPhongProg->addUniform("lightPos");
	fadeSkinPhongProg->addUniform("lightCol");
	fadeSkinPhongProg->addUniform("viewDist");
	fadeSkinPhongProg->addUniform("matAmb");
	fadeSkinPhongProg->addUniform("matDif");
	fadeSkinPhongProg->addUniform("matSpec");
	fadeSkinPhongProg->addUniform("matShine");
	fadeSkinPhongProg->addAttribute("vertPos");
	fadeSkinPhongProg->addAttribute("vertNor");
	fadeSkinPhongProg->addAttribute("weights0");
	fadeSkinPhongProg->addAttribute("weights1");
	fadeSkinPhongProg->addAttribute("bones0");
	fadeSkinPhongProg->addAttribute("bones1");
	fadeSkinPhongProg->addAttribute("numBones");

	// Initialize the fading particle program
	fadeParticleProg = make_shared<Program>();
	fadeParticleProg->setVerbose(true); // Set this to true when debugging.
	fadeParticleProg->setShaderNames(RESOURCE_DIR + "fading_particle_vert.glsl", RESOURCE_DIR + "fading_particle_frag.glsl");
	fadeParticleProg->init();
	fadeParticleProg->addUniform("P");
	fadeParticleProg->addUniform("M");
	fadeParticleProg->addUniform("V");
	fadeParticleProg->addUniform("camPos");
	fadeParticleProg->addUniform("viewDist");
	fadeParticleProg->addAttribute("vertPos");
	fadeParticleProg->addAttribute("vertTex");
	fadeParticleProg->addUniform("radius");
	fadeParticleProg->addUniform("texture");
	fadeParticleProg->addUniform("alphaTexture");
	fadeParticleProg->addUniform("theta");
}

// Sort particles by their z values in camera space
class ParticleSorter {
public:
	bool operator()(size_t i0, size_t i1) const
	{
		// Particle positions in world space
		const Vector3f &x0 = particles[i0]->getPosition().cast<float>();
		const Vector3f &x1 = particles[i1]->getPosition().cast<float>();
		// Particle positions in camera space
		float z0 = V.row(2) * Vector4f(x0(0), x0(1), x0(2), 1.0f);
		float z1 = V.row(2) * Vector4f(x1(0), x1(1), x1(2), 1.0f);
		return z0 < z1;
	}
	
	Matrix4f V; // current view matrix
	vector<shared_ptr<Particle>> particles;
};
ParticleSorter sorter;

// http://stackoverflow.com/questions/1577475/c-sorting-and-keeping-track-of-indexes
template <typename T>
vector<size_t> sortIndices(const vector<T> &v) {
	// initialize original index locations
	vector<size_t> idx(v.size());
	for (size_t i = 0; i != idx.size(); ++i) idx[i] = i;
	// sort indexes based on comparing values in v
	sort(idx.begin(), idx.end(), sorter);
	return idx;
}

static void render()
{
	// Get the frame time.
	double t = glfwGetTime();

	// Get current frame buffer size.
	int width, height;
	glfwGetFramebufferSize(window, &width, &height);
	glViewport(0, 0, width, height);

	// Clear framebuffer.
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   float aspect = width/(float)height;

   // Define the light position in the world.
   Vector3f lightPos(200.0f, 200.0f, -200.0f);
   Vector3f lightCol(1.0f, 1.0f, 1.0f);

   // Create the matrix stacks - please leave these alone for now
   auto P = make_shared<MatrixStack>();
   auto V = make_shared<MatrixStack>();
   auto M = make_shared<MatrixStack>();

   // Get perspective projection.
   P->pushMatrix();
   P->perspective(45.0f, aspect, 0.01f, viewDist);

   // Get view matrix.
   V->pushMatrix();
   Vector3f lookAtPos = camPos + camDir;
   V->lookAt(camPos, lookAtPos, Vector3f(0.0f, 1.0f, 0.0f));

   // Initial push.
   M->pushMatrix();

   /* Draw all objects that
   1. Have skinned meshes
   2. Need to fade in the distance
   3. Need Blinn-Phong lighting
   4. Need programatically defined material properties
   */
   fadeSkinPhongProg->bind();
   glUniformMatrix4fv(fadeSkinPhongProg->getUniform("P"), 1, GL_FALSE, P->topMatrix().data());
   glUniformMatrix4fv(fadeSkinPhongProg->getUniform("V"), 1, GL_FALSE, V->topMatrix().data());
   glUniformMatrix4fv(fadeSkinPhongProg->getUniform("M"), 1, GL_FALSE, M->topMatrix().data());
   glUniform3f(fadeSkinPhongProg->getUniform("camPos"), camPos[0], camPos[1], camPos[2]);
   glUniform3f(fadeSkinPhongProg->getUniform("lightPos"), lightPos[0], lightPos[1], lightPos[2]);
   glUniform3f(fadeSkinPhongProg->getUniform("lightCol"), lightCol(0), lightCol(1), lightCol(2));
   glUniform1f(fadeSkinPhongProg->getUniform("viewDist"), viewDist);

   // Draw sharks.
   setMaterial(5, fadeSkinPhongProg);
   for (int i = 0; i < (int) sharks.size(); i++) {
	   sharks[i]->prep(fadeSkinPhongProg, t);
	   sharks[i]->draw(fadeSkinPhongProg);

	   // See if the shark is firing a missle.
	   int chance = int(100000.0 * (t - t0) * mpsps);
	   int value = (int)randRangef(0.0f, 100000.0f);
	   if (value < chance && (unsigned int)keyToggles['f']) {
		   shared_ptr<Missle> missle = sharks[i]->fireMissle(t);
		   missle->setShape(missleShape);
		   missles.push_back(missle);
	   }
   }
   fadeSkinPhongProg->unbind();

   /* Draw all objects that
   1. Need to wave over time
   2. Need to fade in the distance
   3. Need Blinn-Phong lighting
   4. Need programatically defined material properties.
   */
   fadeWavePhongProg->bind();
   glUniformMatrix4fv(fadeWavePhongProg->getUniform("P"), 1, GL_FALSE, P->topMatrix().data());
   glUniformMatrix4fv(fadeWavePhongProg->getUniform("V"), 1, GL_FALSE, V->topMatrix().data());
   glUniform3f(fadeWavePhongProg->getUniform("camPos"), camPos[0], camPos[1], camPos[2]);
   glUniform3f(fadeWavePhongProg->getUniform("lightPos"), lightPos[0], lightPos[1], lightPos[2]);
   glUniform3f(fadeWavePhongProg->getUniform("lightCol"), lightCol(0), lightCol(1), lightCol(2));
   glUniform3f(fadeWavePhongProg->getUniform("wave"), 1.0f, 0.0f, 0.0f);
   glUniform1f(fadeWavePhongProg->getUniform("viewDist"), viewDist);
   glUniform1f(fadeWavePhongProg->getUniform("t"), (float) t);

   // Draw seaweeds
   setMaterial(7, fadeWavePhongProg);
   for (int i = 0; i < (int)seaweedTransforms.size(); i++) {
	   M->pushMatrix();
	   M->multMatrix(seaweedTransforms[i]);
	   glUniformMatrix4fv(fadeWavePhongProg->getUniform("M"), 1, GL_FALSE, M->topMatrix().data());
	   seaweed->draw(fadeWavePhongProg);
	   M->popMatrix();
   }
   fadeWavePhongProg->unbind();

   /* Draw all objects that
   1. Have diffuse color textures.
   2. Need to fade in the distance
   3. Need Blinn-Phong lighting
   4. Need programatically defined material properties.
   */
   fadeTexPhongProg->bind();
   glUniformMatrix4fv(fadeTexPhongProg->getUniform("P"), 1, GL_FALSE, P->topMatrix().data());
   glUniformMatrix4fv(fadeTexPhongProg->getUniform("V"), 1, GL_FALSE, V->topMatrix().data());
   glUniform3f(fadeTexPhongProg->getUniform("camPos"), camPos[0], camPos[1], camPos[2]);
   glUniform3f(fadeTexPhongProg->getUniform("lightPos"), lightPos[0], lightPos[1], lightPos[2]);
   glUniform3f(fadeTexPhongProg->getUniform("lightCol"), lightCol(0), lightCol(1), lightCol(2));
   glUniform1f(fadeTexPhongProg->getUniform("viewDist"), viewDist);

   // Draw the nautilus's
   rustedMetalTex0->bind(fadeTexPhongProg->getUniform("texture0"), 0);
   setTextureMaterial(3, fadeTexPhongProg);
   for (int i = 0; i < (int)nautilusTransforms.size(); i++) {
	   M->pushMatrix();
	   M->multMatrix(nautilusTransforms[i]);
	   glUniformMatrix4fv(fadeTexPhongProg->getUniform("M"), 1, GL_FALSE, M->topMatrix().data());
	   nautilus->draw(fadeTexPhongProg);
	   M->popMatrix();
   }
   rustedMetalTex0->unbind(0);

   // Draw the submarine's
   rustedMetalTex1->bind(fadeTexPhongProg->getUniform("texture0"), 0);
   setTextureMaterial(4, fadeTexPhongProg);
   for (int i = 0; i < (int)submarineTransforms.size(); i++) {
	   M->pushMatrix();
	   M->multMatrix(submarineTransforms[i]);
	   glUniformMatrix4fv(fadeTexPhongProg->getUniform("M"), 1, GL_FALSE, M->topMatrix().data());
	   submarine->draw(fadeTexPhongProg);
	   M->popMatrix();
   }
   rustedMetalTex1->unbind(0);

   // Draw the rocks:
   coral0Tex->bind(fadeTexPhongProg->getUniform("texture0"), 0);
   setTextureMaterial(2, fadeTexPhongProg);
   for (int i = 0; i < (int)rockTransforms.size(); i++) {
	   shared_ptr<Shape> rock;
	   switch (i) {
	   case 0:
		   rock = rock0;
		   break;
	   case 1:
		   rock = rock1;
		   break;
	   case 2:
		   rock = rock2;
		   coral0Tex->unbind(0);
		   coral1Tex->bind(fadeTexPhongProg->getUniform("texture0"), 0);
		   break;
	   case 3:
		   coral1Tex->unbind(0);
		   coral2Tex->bind(fadeTexPhongProg->getUniform("texture0"), 0);
		   rock = rock3;
		   break;
	   }
	   for (int j = 0; j < (int)rockTransforms[i].size(); j++) {
		   M->pushMatrix();
		   M->multMatrix(rockTransforms[i][j]);
		   glUniformMatrix4fv(fadeTexPhongProg->getUniform("M"), 1, GL_FALSE, M->topMatrix().data());
		   rock->draw(fadeTexPhongProg);
		   M->popMatrix();
	   }
   }
   coral2Tex->unbind(0);

   // Draw the sand.
   sandTex->bind(fadeTexPhongProg->getUniform("texture0"), 0);
   setTextureMaterial(0, fadeTexPhongProg);
   Vector2i gridLL(-20, -20 );
   Vector2i gridUR(20, 20);
   float scale = 10.0f;
   for (int i = gridLL(0); i < gridUR(0); i++) {
	   for (int j = gridLL(1); j < gridUR(1); j++) {
		   M->pushMatrix();
		   M->translate(Vector3f(2.0f * scale * (float)i, 0.0f, 2.0f * scale * (float)j));
		   M->scale(scale);
		   glUniformMatrix4fv(fadeTexPhongProg->getUniform("M"), 1, GL_FALSE, M->topMatrix().data());
		   plane->draw(fadeTexPhongProg);
		   M->popMatrix();
	   }
   }
   sandTex->unbind(0);

   
   // Draw the surface
   glDisable(GL_CULL_FACE);
   surfaceTex->bind(fadeTexPhongProg->getUniform("texture0"), 0);
   setTextureMaterial(1, fadeTexPhongProg);
   for (int i = gridLL(0); i < gridUR(0); i++) {
	   for (int j = gridLL(1); j < gridUR(1); j++) {
		   M->pushMatrix();
		   M->translate(Vector3f(2.0f * scale * (float)i, 30.0f, 2.0f * scale * (float)j));
		   M->scale(scale);
		   glUniformMatrix4fv(fadeTexPhongProg->getUniform("M"), 1, GL_FALSE, M->topMatrix().data());
		   plane->draw(fadeTexPhongProg);
		   M->popMatrix();
	   }
   }
   surfaceTex->unbind(0);
   glEnable(GL_CULL_FACE);
   fadeTexPhongProg->unbind();

   /* Draw all objects that
   1. Need to fade in the distance
   2. Need Blinn-Phong lighting
   3. Need programatically defined material properties.
   */
   fadePhongProg->bind();
   glUniformMatrix4fv(fadePhongProg->getUniform("P"), 1, GL_FALSE, P->topMatrix().data());
   glUniformMatrix4fv(fadePhongProg->getUniform("V"), 1, GL_FALSE, V->topMatrix().data());
   glUniform3f(fadePhongProg->getUniform("camPos"), camPos[0], camPos[1], camPos[2]);
   glUniform3f(fadePhongProg->getUniform("lightPos"), lightPos[0], lightPos[1], lightPos[2]);
   glUniform3f(fadePhongProg->getUniform("lightCol"), lightCol(0), lightCol(1), lightCol(2));
   glUniform1f(fadePhongProg->getUniform("viewDist"), viewDist);
   glUniform1f(fadePhongProg->getUniform("baseAlpha"), 1.0f);

   // Draw missles.
   setMaterial(0, fadePhongProg);
   for (int i = 0; i < (int)missles.size(); i++) {
	   missles[i]->prep(fadePhongProg, t);
	   missles[i]->draw(fadePhongProg);

	   if (missles[i]->checkHit()) {
		   explosion(missles[i]->getPosition(), 8.0f);
		   missles.erase(missles.begin() + i);
	   }
	   else {
		   thrust(missles[i]->getPosition(), missles[i]->getPreviousPosition());
	   }
   }

   // Draw Nefertiti
   M->pushMatrix();
   M->translate(Vector3f(0.0f, 1.5f, 2.0f));
   M->rotate((float)M_PI * 0.33f, Vector3f(-1.0f, 0.0f, 1.0f));
   M->rotate((float)-M_PI * 0.5f, Vector3f(1.0f, 0.0f, 0.0f));
   M->scale(10.0f);
   glUniformMatrix4fv(fadePhongProg->getUniform("M"), 1, GL_FALSE, M->topMatrix().data());
   setMaterial(2, fadePhongProg);
   nefertiti->draw(fadePhongProg);
   M->popMatrix();

   // Draw bubbles.
   glUniform1f(fadePhongProg->getUniform("baseAlpha"), 0.5f);
   setMaterial(6, fadePhongProg);
   for (unsigned int i = 0; i < bubbles.size(); i++) {
	   bubbles[i]->draw(fadePhongProg, t);
   }
   fadePhongProg->unbind();
   
   /* Draw all objects that
   1. Are particles
   2. Need to fade in the distance
   */
   fadeParticleProg->bind();
   glUniformMatrix4fv(fadeParticleProg->getUniform("P"), 1, GL_FALSE, P->topMatrix().data());
   glUniformMatrix4fv(fadeParticleProg->getUniform("V"), 1, GL_FALSE, V->topMatrix().data());
   glUniform3f(fadeParticleProg->getUniform("camPos"), camPos[0], camPos[1], camPos[2]);
   glUniform1f(fadeParticleProg->getUniform("viewDist"), viewDist);
   Particle::bind(fadeParticleProg);
   sorter.V = V->topMatrix();

   // Exploding rock particles first.
   rockParticleTex->bind(fadeParticleProg->getUniform("texture"), 0);
   rockParticleAlphaTex->bind(fadeParticleProg->getUniform("alphaTexture"), 1);
   sorter.particles = rockParticles;
	auto sortedIndices = sortIndices(rockParticles);
	for (unsigned int i = 0; i < sortedIndices.size(); ++i) {
		int ii = sortedIndices[i];
		rockParticles[ii]->draw(fadeParticleProg, M);
	}
	rockParticleTex->unbind(0);
	rockParticleAlphaTex->unbind(1);

	// Then thrust particles.
	bubbleTex->bind(fadeParticleProg->getUniform("texture"), 0);
	bubbleAlphaTex->bind(fadeParticleProg->getUniform("alphaTexture"), 1);
	sorter.particles = thrustParticles;
	sortedIndices = sortIndices(thrustParticles);
	for (unsigned int i = 0; i < sortedIndices.size(); ++i) {
		int ii = sortedIndices[i];
		thrustParticles[ii]->draw(fadeParticleProg, M);
	}
	bubbleTex->unbind(0);
	bubbleAlphaTex->unbind(1);
	Particle::unbind(fadeParticleProg);
	fadeParticleProg->unbind();

   // Pop matrix stacks.
   M->popMatrix();
   V->popMatrix();
   P->popMatrix();

   // Set previous frame time.
   t0 = t;
}

void stepParticles()
{
	// Calculate the time that has passed
	double t = glfwGetTime();
	double dt = t - t0;

	// Advance all rock particles
	#pragma omp for
	for (unsigned int i = 0; i < rockParticles.size(); i++) {
		Vector3d velocity = rockParticles[i]->getVelocity();
		Vector3d position = rockParticles[i]->getPosition();
		float theta = rockParticles[i]->getAngle();
		float dTheta = rockParticles[i]->getAngularVelocity();

		velocity[0] += dt * (-0.9 * velocity[0]);
		velocity[1] += dt * (-0.9 * velocity[1] - 9.8);
		velocity[2] += dt * (-0.9 * velocity[2]);

		position[0] += dt * velocity[0];
		position[1] += dt * velocity[1];
		position[2] += dt * velocity[2];

		dTheta += float(dt) * (-0.9f * dTheta);
		theta += float(dt) * dTheta;

		rockParticles[i]->setVelocity(velocity);
		rockParticles[i]->setPosition(position);
		rockParticles[i]->setAngle(theta);
		rockParticles[i]->setAngularVelocity(dTheta);
	}

	// Advance all thrust particles
	#pragma omp for
	for (unsigned int i = 0; i < thrustParticles.size(); i++) {
		Vector3d velocity = thrustParticles[i]->getVelocity();
		Vector3d position = thrustParticles[i]->getPosition();

		velocity[0] += dt * (-0.1 * velocity[0]);
		velocity[1] += dt * (-0.1 * velocity[1]);
		velocity[2] += dt * (-0.1 * velocity[2]);

		position[0] += dt * velocity[0];
		position[1] += dt * velocity[1];
		position[2] += dt * velocity[2];

		thrustParticles[i]->setVelocity(velocity);
		thrustParticles[i]->setPosition(position);
	}

	// Erase any rock particles that are below the sand surface
	for (int i = (int)rockParticles.size() - 1; i >= 0; i--) {
		shared_ptr<Particle> particle = rockParticles[i];
		if (particle->getPosition()[1] < 0.0) {
			rockParticles.erase(rockParticles.begin() + i);
		}
	}

	// Erase any thrust particles that have lived past their duration
	for (int i = (int)thrustParticles.size() - 1; i >= 0; i--) {
		shared_ptr<Particle> particle = thrustParticles[i];
		if (!particle->isAlive(t)) {
			thrustParticles.erase(thrustParticles.begin() + i);
		}
	}
}

int main(int argc, char **argv)
{
	if(argc < 2) {
		cout << "Please specify the resource directory." << endl;
		return 0;
	}
	RESOURCE_DIR = argv[1] + string("/");

	// Set error callback.
	glfwSetErrorCallback(error_callback);
	// Initialize the library.
	if(!glfwInit()) {
		return -1;
	}
   //request the highest possible version of OGL - important for mac
   glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
   glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
   glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
   glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);

	// Create a windowed mode window and its OpenGL context.
	window = glfwCreateWindow(1200, 800, "Keenan M. Reimer", NULL, NULL);
	if(!window) {
		glfwTerminate();
		return -1;
	}
	// Make the window's context current.
	glfwMakeContextCurrent(window);
	// Initialize GLEW.
	glewExperimental = true;
	if(glewInit() != GLEW_OK) {
		cerr << "Failed to initialize GLEW" << endl;
		return -1;
	}
	//weird bootstrap of glGetError
   glGetError();
	cout << "OpenGL version: " << glGetString(GL_VERSION) << endl;
   cout << "GLSL version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;

	// Set vsync.
	glfwSwapInterval(1);
	// Set keyboard callback.
	glfwSetKeyCallback(window, key_callback);
	// Set char callback.
	glfwSetCharCallback(window, char_callback);
   //set the mouse call back
   glfwSetMouseButtonCallback(window, mouse_callback);
   // Set cursor position callback.
   glfwSetCursorPosCallback(window, cursor_pos_callback);
   //set the window resize call back
   glfwSetFramebufferSizeCallback(window, resize_callback);

	// Initialize scene. Note geometry initialized in init now
	init();

	// Loop until the user closes the window.
	while(!glfwWindowShouldClose(window)) {
	  // step any particles.
	  stepParticles();
		// Render scene.
		render();
		// Swap front and back buffers.
		glfwSwapBuffers(window);
		// Poll for and process events.
		glfwPollEvents();
	}
	// Quit program.
	glfwDestroyWindow(window);
	glfwTerminate();
	return 0;
}
