#pragma once
#ifndef _MISSLE_H_
#define _MISSLE_H_

#include "Shape.h"
#include "MatrixStack.h"
#include "Program.h"
#include "Utilities.h"

using namespace std;
using namespace Eigen;

class Missle {
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	Missle();
	Missle(shared_ptr<Shape> missleShape);
	~Missle();

	void setInitialPosition(Vector3f initPos);
	void setInitialVelocity(Vector3f initVel);
	void setInitialTime(double initTime);
	void setShape(shared_ptr<Shape> newShape);

	bool checkHit();
	const Vector3f getPosition() { return p; }
	const Vector3f getPreviousPosition() { return prevP; }
	const Vector3f getVelocity() { return v;  }

	void prep(const shared_ptr<Program> prog, const double t);
	void draw(const shared_ptr<Program> prog);

private:
	Vector3f prevP;
	Vector3f p;
	Vector3f v;
	float t0;

	shared_ptr<Shape> shape;
	shared_ptr<MatrixStack> M;
};

#endif
