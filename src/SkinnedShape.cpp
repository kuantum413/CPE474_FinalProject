#include "SkinnedShape.h"
#include <iostream>
#include <sstream>
#include <fstream>

#include "GLSL.h"
#include "Program.h"

#include "tiny_obj_loader.h"

using namespace std;
using namespace Eigen;

SkinnedShape::SkinnedShape() :
	eleBufID(0),
	posBufID(0),
	norBufID(0),
	newPosBufID(0),
	newNorBufID(0),
	texBufID(0),
	weiBufID(0),
	inxBufID(0),
	numBufID(0)
{
}

SkinnedShape::~SkinnedShape()
{
}

void SkinnedShape::loadMesh(const string &meshName)
{
	// Load geometry
	// Some obj files contain material information.
	// We'll ignore them for this assignment.
	vector<tinyobj::shape_t> shapes;
	vector<tinyobj::material_t> objMaterials;
	string errStr;
	bool rc = tinyobj::LoadObj(shapes, objMaterials, errStr, meshName.c_str());
	if (!rc) {
		cerr << errStr << endl;
	}
	else {
		posBuf = shapes[0].mesh.positions;
		//norBuf = shapes[0].mesh.normals;
		texBuf = shapes[0].mesh.texcoords;
		eleBuf = shapes[0].mesh.indices;

		computeNormals();
	}
}

void SkinnedShape::computeNormals() {
	// Compute the vertex normals manually by looking at each face.
	norBuf.resize(posBuf.size());
	for (int i = 0; i < (int) eleBuf.size() / 3; i++) {
		// Grab the vertex indices for each face.
		vector<float> vertIndices(eleBuf.begin() + 3 * i, eleBuf.begin() + 3 * i + 3);

		// Use the indices to get the vertex positions.
		vector<Vector3f> verts;
		for (int j = 0; j < 3; j++) {
			Vector3f vert(posBuf[3 * (int)vertIndices[j]], posBuf[3 * (int)vertIndices[j] + 1], posBuf[3 * (int)vertIndices[j] + 2]);
			verts.push_back(vert);
		}

		// Use the distances between the vertices to calculate the normal.
		Vector3f v1 = verts[1] - verts[0];
		Vector3f v2 = verts[2] - verts[0];
		Vector3f norm = v1.cross(v2);

		// Add the normal value to the values stored in the normal buffer for each vertex.
		for (int j = 0; j < 3; j++) {
			norBuf[3 * (int)vertIndices[j]] += norm(0);
			norBuf[3 * (int)vertIndices[j] + 1] += norm(1);
			norBuf[3 * (int)vertIndices[j] + 2] += norm(2);
		}
	}

	// Go back through and normalize all of the vertices.
	for (int i = 0; i < (int) norBuf.size() / 3; i++) {
		float mag = sqrt(pow(norBuf[3 * i], 2) + pow(norBuf[3 * i + 1], 2) + pow(norBuf[3 * i + 2], 2));
		norBuf[3 * i] = norBuf[3 * i] / mag;
		norBuf[3 * i + 1] = norBuf[3 * i + 1] / mag;
		norBuf[3 * i + 2] = norBuf[3 * i + 2] / mag;
	}
}

void SkinnedShape::loadWeights(const std::string &filename)
{
	ifstream in;
	in.open(filename.c_str());
	if (!in.good()) {
		std::cout << "Cannot read " << filename.c_str() << endl;
		return;
	}

	// Skip leading comments.
	string line;
	getline(in, line);
	while (line.at(0) == '#') {
		getline(in, line);
	}

	// Parse line for dimensions
	stringstream ss(line);
	int nrows;
	ss >> nrows;
	ss >> nBones;
	weiBuf.clear();
	weiBuf.resize(nrows * 16);

	inxBuf.clear();
	inxBuf.resize(nrows * 16);
	
	numBuf.clear();
	numBuf.resize(nrows);
	fill(numBuf.begin(), numBuf.end(), 0.0f);

	int row = 0;
	while (1) {
		getline(in, line);
		if (in.eof()) {
			break;
		}
		// Skip empty lines
		if (line.size() < 2) {
			continue;
		}
		// Skip comments
		if (line.at(0) == '#') {
			continue;
		}
		// Parse line
		stringstream ss2(line);
		int num = 0;  // Store how many bones influence this vertex.
		float weight;  // Placeholder for weight values.
		for (int i = 0; i < nBones; i++) {  // For each bone,
			ss2 >> weight;  // grab its weight,
			 if (weight > 0.01f && num < 16) {  // and if it  influences the vertex,
			   weiBuf[16 * row + num] = weight;  // add its weight to the weight buffer,
			   inxBuf[16 * row + num] = (float) i;  // add its index to the index buffer,
			   num ++;  // and increment the number of influencing bones.
			 }
		}
		numBuf[row] = (float) num;  // Record how many bones influence this vertex.
		row++;
	}
	in.close();
}

void SkinnedShape::loadAnimation(const std::string &filename)
{
	ifstream in;
	in.open(filename.c_str());
	if (!in.good()) {
		std::cout << "Cannot read " << filename.c_str() << endl;
		return;
	}

	// Skip leading comments.
	string line;
	getline(in, line);
	while (line.at(0) == '#') {
		getline(in, line);
	}

	// Prepare the new buffer.
	vector<Matrix4f> poseBuf;

	// Parse first line for dimensions
	stringstream ss(line);
	int ncols;
	int frameCount;
	ss >> frameCount;
	frameCount -= 1;  // Do not count the bind pose.
	frameCounts.push_back(frameCount);

	ss >> ncols;
	poseBuf.resize(frameCount*ncols);
	poseBuf.clear();

	// Parse next line for the bind pose.
	getline(in, line);
	stringstream ss2(line);
	for (int i = 0; i < ncols; i++) {
		float rX, rY, rZ, rW, pX, pY, pZ;
		ss2 >> rX;
		ss2 >> rY;
		ss2 >> rZ;
		ss2 >> rW;
		ss2 >> pX;
		ss2 >> pY;
		ss2 >> pZ;

		Matrix4f bone = Matrix4f::Identity();
		Quaternionf q(rW, rX, rY, rZ);
		Vector3f p(pX, pY, pZ);

		bone.block<3, 3>(0, 0) = q.toRotationMatrix();
		bone.block<3, 1>(0, 3) = p;
		bindBuf.push_back(bone.inverse());  // Add the inverse to save computation time later.
	}

	// Parse the rest of the file for all of the other frames.
	while (1) {
		getline(in, line);
		if (in.eof()) {
			break;
		}
		// Skip empty lines
		if (line.size() < 2) {
			continue;
		}
		// Skip comments
		if (line.at(0) == '#') {
			continue;
		}
		// Parse line
		stringstream ss3(line);
		for (int i = 0; i < ncols; i++) {
			float rX, rY, rZ, rW, pX, pY, pZ;
			ss3 >> rX;
			ss3 >> rY;
			ss3 >> rZ;
			ss3 >> rW;
			ss3 >> pX;
			ss3 >> pY;
			ss3 >> pZ;

			Matrix4f bone = Matrix4f::Identity();
			Quaternionf q(rW, rX, rY, rZ);
			Vector3f p(pX, pY, pZ);

			bone.block<3, 3>(0, 0) = q.toRotationMatrix();
			bone.block<3, 1>(0, 3) = p;
			poseBuf.push_back(bone);
		}
	}
	in.close();
	poses.push_back(poseBuf);  // Add the new pose.
}

void SkinnedShape::init()
{
	//for (int num = 0; num < (int)posBuf.size() / 3; num++) {
	//	cout << "Vertex: " << num << endl;
	//	cout << "NumBones: " << numBuf[num] << endl;
	//	float sum = 0.0f;
	//	for (int i = 0; i < nBones; i++) {
	//		//cout << "Bind M0: " << i << endl << bindBuf[i] << endl;
	//		//cout << " M: " << i << endl << poses[0][nBones * num + i] << endl;
	//		cout << "Weight: " << i << endl << weiBuf[nBones * num + i] << endl;
	//		cout << "Index: " << i << endl << inxBuf[nBones * num + i] << endl;
	//		sum += weiBuf[nBones * num + i];
	//	}
	//	cout << "Weight Sum: " << sum << endl;
	//}

	// Send the position array to the GPU
	glGenBuffers(1, &posBufID);
	glBindBuffer(GL_ARRAY_BUFFER, posBufID);
	glBufferData(GL_ARRAY_BUFFER, posBuf.size()*sizeof(float), &posBuf[0], GL_STATIC_DRAW);
	glGenBuffers(1, &newPosBufID);
	
	// Send the normal array to the GPU
	if(!norBuf.empty()) {
		glGenBuffers(1, &norBufID);
		glBindBuffer(GL_ARRAY_BUFFER, norBufID);
		glBufferData(GL_ARRAY_BUFFER, norBuf.size()*sizeof(float), &norBuf[0], GL_STATIC_DRAW);
		glGenBuffers(1, &newNorBufID);
	}
	
	// Send the texture array to the GPU
	if(!texBuf.empty()) {
		glGenBuffers(1, &texBufID);
		glBindBuffer(GL_ARRAY_BUFFER, texBufID);
		glBufferData(GL_ARRAY_BUFFER, texBuf.size()*sizeof(float), &texBuf[0], GL_STATIC_DRAW);
	}

	// Generate an array ID for the vertex bone weights.
	if (!weiBuf.empty()) {
		glGenBuffers(1, &weiBufID);
		glBindBuffer(GL_ARRAY_BUFFER, weiBufID);
		//for (int i = 0; i < weiBuf.size(); i++) {
		//  int vertex = i / 16;
		//  int nBones = numBuf[vertex];
		//  int bone = inxBuf[i];
		//  float weight = weiBuf[i];
		//  //if (weight > 0.0f) {
		//  //  cout << vertex << ": " << nBones << ": " << bone << ": " << weight << endl;
		//  //}
		//}
		glBufferData(GL_ARRAY_BUFFER, weiBuf.size()*sizeof(float), &weiBuf[0], GL_STATIC_DRAW);
	}

	// Generate an array ID for the vertex bone weight indices.
	if (!inxBuf.empty()) {
		glGenBuffers(1, &inxBufID);
		glBindBuffer(GL_ARRAY_BUFFER, inxBufID);
		glBufferData(GL_ARRAY_BUFFER, inxBuf.size()*sizeof(float), &inxBuf[0], GL_STATIC_DRAW);
	}


	// Generate an array ID for the number of vertex bone weights.
	if (!numBuf.empty()) {
		glGenBuffers(1, &numBufID);
		glBindBuffer(GL_ARRAY_BUFFER, numBufID);
		glBufferData(GL_ARRAY_BUFFER, numBuf.size()*sizeof(float), &numBuf[0], GL_STATIC_DRAW);
	}

	// Send the element array to the GPU
	glGenBuffers(1, &eleBufID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eleBufID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, eleBuf.size()*sizeof(unsigned int), &eleBuf[0], GL_STATIC_DRAW);
	
	// Unbind the arrays
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void SkinnedShape::prepCPU(const shared_ptr<Program> prog, int index, float t) const {
	int k = (int) floor(fmod(100.0f * t, frameCounts[index]));
	vector<float> newPosBuf;
	vector<float> newNorBuf;
	newPosBuf.resize(posBuf.size());
	newNorBuf.resize(norBuf.size());
	for (int i = 0; i < (int) posBuf.size() / 3; i++) {
		Vector4f x0(posBuf[3 * i], posBuf[3 * i + 1], posBuf[3 * i + 2], 1.0f);
		Vector4f n0(norBuf[3 * i], norBuf[3 * i + 1], norBuf[3 * i + 2], 0.0f);
		Vector4f x(0.0f, 0.0f, 0.0f, 1.0f);
		Vector4f n(0.0f, 0.0f, 0.0f, 0.0f);

		for (int a = 0; a < numBuf[i]; a++) {
			int j = (int) inxBuf[16 * i + a];
			x += weiBuf[16 * i + a] * poses[index][nBones * k + j] * bindBuf[j] * x0;
			n += weiBuf[16 * i + a] * poses[index][nBones * k + j] * bindBuf[j] * n0;
		}

		newPosBuf[3 * i] = x[0];
		newPosBuf[3 * i + 1] = x[1];
		newPosBuf[3 * i + 2] = x[2];
		newNorBuf[3 * i] = n[0];
		newNorBuf[3 * i + 1] = n[1];
		newNorBuf[3 * i + 2] = n[2];
	}

	// Send the updated position array to the GPU
	glBindBuffer(GL_ARRAY_BUFFER, newPosBufID);
	glBufferData(GL_ARRAY_BUFFER, newPosBuf.size()*sizeof(float), &newPosBuf[0], GL_STATIC_DRAW);

	// Send the updated normal array to the GPU
	if (!newNorBuf.empty()) {
		glBindBuffer(GL_ARRAY_BUFFER, newNorBufID);
		glBufferData(GL_ARRAY_BUFFER, newNorBuf.size()*sizeof(float), &newNorBuf[0], GL_STATIC_DRAW);
	}

	// Unbind the arrays
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void SkinnedShape::drawCPU(const shared_ptr<Program> prog) const
{
	// Bind position buffer
	int h_pos = prog->getAttribute("vertPos");
	GLSL::enableVertexAttribArray(h_pos);
	glBindBuffer(GL_ARRAY_BUFFER, newPosBufID);
	glVertexAttribPointer(h_pos, 3, GL_FLOAT, GL_FALSE, 0, (const void *)0);
	
	// Bind normal buffer
	int h_nor = prog->getAttribute("vertNor");
	if(h_nor != -1 && newNorBufID != 0) {
		GLSL::enableVertexAttribArray(h_nor);
		glBindBuffer(GL_ARRAY_BUFFER, newNorBufID);
		glVertexAttribPointer(h_nor, 3, GL_FLOAT, GL_FALSE, 0, (const void *)0);
	}
	
	// Bind element buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eleBufID);
	
	// Draw
	glDrawElements(GL_TRIANGLES, (int)eleBuf.size(), GL_UNSIGNED_INT, (const void *)0);
	
	if(h_nor != -1) {
		GLSL::disableVertexAttribArray(h_nor);
	}
	GLSL::disableVertexAttribArray(h_pos);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void SkinnedShape::prepGPU(const shared_ptr<Program> prog, int index, float t) const {
	// Send the bind pose to the GPU.
	glUniformMatrix4fv(prog->getUniform("M0"), 6, GL_FALSE, bindBuf[0].data());

	// Send the frame bones to the GPU.
	int k = (int) floor(fmod(100.0f * t, frameCounts[index]));
	vector<Matrix4f>::const_iterator start = poses[index].begin() + nBones * k;
	vector<Matrix4f>::const_iterator end = start + nBones;
	vector<Matrix4f> M(start, end);
	glUniformMatrix4fv(prog->getUniform("M1"), 6, GL_FALSE, M[0].data());
}

void SkinnedShape::drawGPU(const shared_ptr<Program> prog) const
{
	// Bind position buffer
	int h_pos = prog->getAttribute("vertPos");
	GLSL::enableVertexAttribArray(h_pos);
	glBindBuffer(GL_ARRAY_BUFFER, posBufID);
	glVertexAttribPointer(h_pos, 3, GL_FLOAT, GL_FALSE, 0, (const void *)0);

	// Bind normal buffer
	int h_nor = prog->getAttribute("vertNor");
	GLSL::enableVertexAttribArray(h_nor);
	glBindBuffer(GL_ARRAY_BUFFER, norBufID);
	glVertexAttribPointer(h_nor, 3, GL_FLOAT, GL_FALSE, 0, (const void *)0);

	// Bind weights buffers
	int h_wei0 = prog->getAttribute("weights0");
	int h_wei1 = prog->getAttribute("weights1");
	GLSL::enableVertexAttribArray(h_wei0);
	GLSL::enableVertexAttribArray(h_wei1);
	glBindBuffer(GL_ARRAY_BUFFER, weiBufID);
	unsigned stride = 8 * sizeof(float);
	glVertexAttribPointer(h_wei0, 4, GL_FLOAT, GL_FALSE, stride, (const void *)(0 * sizeof(float)));
	glVertexAttribPointer(h_wei1, 4, GL_FLOAT, GL_FALSE, stride, (const void *)(4 * sizeof(float)));

	// Bind index buffers
	int h_inx0 = prog->getAttribute("bones0");
	int h_inx1 = prog->getAttribute("bones1");
	GLSL::enableVertexAttribArray(h_inx0);
	GLSL::enableVertexAttribArray(h_inx1);
	glBindBuffer(GL_ARRAY_BUFFER, inxBufID);
	glVertexAttribPointer(h_inx0, 4, GL_FLOAT, GL_FALSE, stride, (const void *)(0 * sizeof(float)));
	glVertexAttribPointer(h_inx1, 4, GL_FLOAT, GL_FALSE, stride, (const void *)(4 * sizeof(float)));

	// Bind number of bones buffer
	int h_num = prog->getAttribute("numBones");
	GLSL::enableVertexAttribArray(h_num);
	glBindBuffer(GL_ARRAY_BUFFER, numBufID);
	glVertexAttribPointer(h_num, 1, GL_FLOAT, GL_FALSE, sizeof(float), (const void *)0);

	// Bind element buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eleBufID);

	// Draw
	glDrawElements(GL_TRIANGLES, (int)eleBuf.size(), GL_UNSIGNED_INT, (const void *)0);

	// Disable and unbind
	GLSL::disableVertexAttribArray(h_nor);
	GLSL::disableVertexAttribArray(h_pos);
	GLSL::disableVertexAttribArray(h_wei0);
	GLSL::disableVertexAttribArray(h_wei1);
	GLSL::disableVertexAttribArray(h_inx0);
	GLSL::disableVertexAttribArray(h_inx1);
	GLSL::disableVertexAttribArray(h_num);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void SkinnedShape::setBindPose(const std::vector<Matrix4f> buf) {
	bindBuf = buf;
	nBones = (int)buf.size();
}

void SkinnedShape::addAnimation(const std::vector<Eigen::Matrix4f> anim) {
	int nFrames = (int)anim.size() / nBones;
	frameCounts.push_back(nFrames);

	poses.push_back(anim);
}

void SkinnedShape::computeWeightsByGeometry(const float maxDist, const float exp) {
	int numVertices = (int)posBuf.size() / 3;
	weiBuf.resize(numVertices * 8);
	inxBuf.resize(numVertices * 8);
	numBuf.resize(numVertices);

	// Loop through every vertex.
	float sum = 0.0f;
	for (int i = 0; i < numVertices; i++) {
		Vector3f vertex(posBuf[3 * i], posBuf[3 * i + 1], posBuf[3 * i + 2]);

		// And calulate its weight by exponential distance.
		int num = 0;
		for (int j = 0; j < 8; j++) {
			if (j < nBones) {
				Matrix4f bone = bindBuf[j];
				Vector3f bonePos = bone.block<3, 1>(0, 3);
				float dist = (bonePos - vertex).norm();
				if (dist < abs(maxDist)) {
					float w = pow(dist, exp);
					weiBuf[8 * i + num] = w;
					inxBuf[8 * i + num] = (float)j;
					sum += w;
					num += 1;
				}
			}
		}

		// Go back through and normalize the weights.
		for (int j = 0; j < num; j++) {
			weiBuf[8 * i + j] *= 1.0f / sum;
		}
		numBuf[i] = float(num);

		sum = 0.0f;
	}
}


