#include <iostream>
#include <stdlib.h>
#include <sstream>

#include "Particle.h"
#include "GLSL.h"
#include "MatrixStack.h"
#include "Program.h"
#include "Texture.h"
#include "Utilities.h"

using namespace std;
using namespace Eigen;

GLuint Particle::posBufID(0);
GLuint Particle::texBufID(0);
GLuint Particle::indBufID(0);

vector<float> Particle::posBuf;
vector<float> Particle::texBuf;
vector<unsigned int> Particle::indBuf;

Particle::Particle() :
	x(0.0, 0.0, 0.0),
	v(0.0, 0.0, 0.0),
	r(0.0f),
	theta(0.0f),
	w(0.0f),
	rV(0.0f),
	t0(0.0),
	life(0.0)
{
	// Random values
	x << randRange(-1.0, 1.0), randRange(-1.0, 1.0), randRange(-1.0, 1.0);
	v << randRange(-1.0, 1.0), randRange(-1.0, 1.0), randRange(-1.0, 1.0);
	r = randRangef(0.1f, 0.3f);
	theta = randRangef(0.0f, 2.0f * (float)M_PI);
	t0 = 0.0;
	life = 0.0f;
}

Particle::~Particle()
{
}

void Particle::init()
{
	posBuf.clear();
	texBuf.clear();
	indBuf.clear();

	// Load geometry
	// 0
	posBuf.push_back(-1.0);
	posBuf.push_back(-1.0);
	posBuf.push_back(0.0);
	texBuf.push_back(0.0);
	texBuf.push_back(0.0);
	// 1
	posBuf.push_back(1.0);
	posBuf.push_back(-1.0);
	posBuf.push_back(0.0);
	texBuf.push_back(1.0);
	texBuf.push_back(0.0);
	// 2
	posBuf.push_back(-1.0);
	posBuf.push_back(1.0);
	posBuf.push_back(0.0);
	texBuf.push_back(0.0);
	texBuf.push_back(1.0);
	// 3
	posBuf.push_back(1.0);
	posBuf.push_back(1.0);
	posBuf.push_back(0.0);
	texBuf.push_back(1.0);
	texBuf.push_back(1.0);
	// indices
	indBuf.push_back(0);
	indBuf.push_back(1);
	indBuf.push_back(2);
	indBuf.push_back(3);
	
	// Send the position array to the GPU
	glGenBuffers(1, &posBufID);
	glBindBuffer(GL_ARRAY_BUFFER, posBufID);
	glBufferData(GL_ARRAY_BUFFER, posBuf.size()*sizeof(float), &posBuf[0], GL_STATIC_DRAW);
	
	// Send the texture coordinates array to the GPU
	glGenBuffers(1, &texBufID);
	glBindBuffer(GL_ARRAY_BUFFER, texBufID);
	glBufferData(GL_ARRAY_BUFFER, texBuf.size()*sizeof(float), &texBuf[0], GL_STATIC_DRAW);
	
	// Send the index array to the GPU
	glGenBuffers(1, &indBufID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indBufID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indBuf.size()*sizeof(unsigned int), &indBuf[0], GL_STATIC_DRAW);
	
	// Unbind the arrays
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	
	assert(glGetError() == GL_NO_ERROR);
}

bool Particle::isAlive(double t) {
  if (t - t0 < life) {
    return true;
  }
  else {
    return false;
  }
}

void Particle::bind(shared_ptr<Program> prog) {
	// Enable and bind position array for drawing
	GLint h_pos = prog->getAttribute("vertPos");
	GLSL::enableVertexAttribArray(h_pos);
	glBindBuffer(GL_ARRAY_BUFFER, posBufID);
	glVertexAttribPointer(h_pos, 3, GL_FLOAT, GL_FALSE, 0, 0);
	
	// Enable and bind texcoord array for drawing
	GLint h_tex = prog->getAttribute("vertTex");
	GLSL::enableVertexAttribArray(h_tex);
	glBindBuffer(GL_ARRAY_BUFFER, texBufID);
	glVertexAttribPointer(h_tex, 2, GL_FLOAT, GL_FALSE, 0, 0);
	
	// Bind index array for drawing
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indBufID);
}

void Particle::draw(shared_ptr<Program> prog, shared_ptr<MatrixStack> M)
{	
	// Transformation matrix
	M->pushMatrix();
	M->translate(x.cast<float>());
	glUniformMatrix4fv(prog->getUniform("M"), 1, GL_FALSE, M->topMatrix().data());
	M->popMatrix();
	
	// Color and scale
	glUniform1f(prog->getUniform("theta"), theta);
	glUniform1f(prog->getUniform("radius"), r);
	
	// Draw
	glDrawElements(GL_TRIANGLE_STRIP, (int)indBuf.size(), GL_UNSIGNED_INT, 0);  
}

void Particle::unbind(shared_ptr<Program> prog) {
	// Disable and unbind
  GLint h_pos = prog->getAttribute("vertPos");
  GLint h_tex = prog->getAttribute("vertTex");
  GLSL::disableVertexAttribArray(h_tex);
  GLSL::disableVertexAttribArray(h_pos);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
