#pragma once
#ifndef _SHARK_H_
#define _SHARK_H_

#include "SkinnedShape.h"
#include "MatrixStack.h"
#include "Program.h"
#include "Utilities.h"
#include "Missle.h"

using namespace std;
using namespace Eigen;

class Shark {
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	Shark();
	Shark(shared_ptr<SkinnedShape> sharkShape);
	~Shark();

	void setHeight(float height);
	void setRadius(float radius);
	void setSpeed(float speed);
	void setCenter(Vector3f center);

	shared_ptr<Missle> fireMissle(double t);
	void prep(const shared_ptr<Program> prog, double t);
	void draw(const shared_ptr<Program> prog);

	static void setBones(shared_ptr<SkinnedShape> sharkShape);
	static void setAnimation(shared_ptr<SkinnedShape> sharkShape);

private:
	Vector3f center;
	float radius;
	float angularSpeed;
	float height;
	float speed;
	float thetaOffset;
	float phiOffset;
	float t0;

	shared_ptr<SkinnedShape> shape;
	shared_ptr<MatrixStack> M;
	Matrix4f E;
};

#endif
