#pragma once
#ifndef _SKINNED_SHAPE_H_
#define _SKINNED_SHAPE_H_

#include <string>
#include <vector>
#include <memory>

#define EIGEN_DONT_ALIGN_STATICALLY
#include <Eigen/Dense>

class Program;

class SkinnedShape
{
public:
	SkinnedShape();
	virtual ~SkinnedShape();
	void loadMesh(const std::string &meshName);
	void loadWeights(const std::string &filename);
	void loadAnimation(const std::string &filename);
	void init();
	void computeNormals();
	void prepCPU(const std::shared_ptr<Program> prog, int index, float t) const;
	void drawCPU(const std::shared_ptr<Program> prog) const;
	void prepGPU(const std::shared_ptr<Program> prog, int index, float t) const;
	void drawGPU(const std::shared_ptr<Program> prog) const;

	void setBindPose(const std::vector<Eigen::Matrix4f> buf);
	void addAnimation(const std::vector<Eigen::Matrix4f> anim);
	void computeWeightsByGeometry(const float maxDist, const float exp);
	
private:
	int nBones;
	std::vector<int> frameCounts;
	std::vector<unsigned int> eleBuf;
	std::vector<float> posBuf;
	std::vector<float> norBuf;
	std::vector<float> texBuf;
	std::vector<float> weiBuf;
	std::vector<float> inxBuf;
	std::vector<float> numBuf;
	std::vector<std::vector<Eigen::Matrix4f>> poses;
	std::vector<Eigen::Matrix4f> bindBuf;
	unsigned eleBufID;
	unsigned posBufID;
	unsigned norBufID;
	unsigned newPosBufID;
	unsigned newNorBufID;
	unsigned texBufID;
	unsigned weiBufID;
	unsigned inxBufID;
	unsigned numBufID;
};

#endif
