#version  330 core
layout(location = 0) in vec3 vertPos;
layout(location = 1) in vec3 vertNor;
in vec4 weights0;
in vec4 weights1;
in vec4 bones0;
in vec4 bones1;
in float numBones;
uniform mat4 M0[6];
uniform mat4 M1[6];

uniform vec3 lightPos;
uniform vec3 camPos;
uniform mat4 P;
uniform mat4 V;
uniform mat4 M;

out vec3 fragNorRaw;
out vec3 lightVecRaw;
out vec3 halfVecRaw;
out vec3 vertPosWorld;
out float dist;

void main()
{
	// Calculate vertex position & normal direction with skinning.
	int num = int(numBones);
	vec4 newVertPos = vec4(0.0f, 0.0f, 0.0f, 1.0f);
	vec4 newVertNor = vec4(0.0f, 0.0f, 0.0f, 0.0f);

	for (int a = 0; a < num; a++) {
		float w = 0.0f;
		int j = 0;
		if (a < 4) {
			w = weights0[a];
			j = int(bones0[a]);
		}
		else if (a < 8) {
			w = weights1[a - 4];
			j = int(bones1[a - 4]);
		}

		newVertPos += w * M1[j] * M0[j] * vec4(vertPos, 1.0f);
		newVertNor += w * M1[j] * M0[j] * vec4(vertNor, 0.0f);
	}
	newVertPos[3] = 1.0f;

	gl_Position = P * V * M * newVertPos;
	fragNorRaw = (M * newVertNor).xyz;

	// Calculate blinn phong lighting parameters for the fragment shader.
	vertPosWorld = (M * newVertPos).xyz;
	vec3 viewVec = normalize(camPos - vertPosWorld);
	lightVecRaw = normalize(lightPos - vertPosWorld);
	halfVecRaw = 0.5f * (viewVec + lightVecRaw);

	// Calculate dist for fading.
	vec3 camDist = camPos - vertPosWorld;
	dist = length(vec2(camDist.x, camDist.z));
}
