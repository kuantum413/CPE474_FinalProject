#version 330 core
in vec2 fragTex;
in float dist;
uniform sampler2D texture;
uniform sampler2D alphaTexture;
uniform float viewDist;
out vec4 color;

void main()
{
	vec3 tex = texture2D(texture, fragTex).rgb;
	float alpha = texture2D(alphaTexture, fragTex).r;

	// Compute Fade alpha.
	float fadeAlpha = 1.0f;
	float fadeBegin = 0.5f * viewDist;
	float fadeEnd = viewDist;
	if (dist > fadeBegin) {
	   fadeAlpha = 1.0f - (dist - fadeBegin)/(fadeEnd - fadeBegin);
	}

	color = vec4(tex, fadeAlpha * alpha);
}
