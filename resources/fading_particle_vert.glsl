#version 330 core
in vec4 vertPos;
in vec2 vertTex;
uniform mat4 P;
uniform mat4 M;
uniform mat4 V;
uniform vec3 camPos;
uniform float radius;
uniform float theta;
out vec2 fragTex;
out float dist;

void main()
{
	// Billboarding: set the upper 3x3 to be the identity matrix
	mat4 MV0 = V * M;
	MV0[0] = vec4(1.0, 0.0, 0.0, 0.0);
	MV0[1] = vec4(0.0, 1.0, 0.0, 0.0);
	MV0[2] = vec4(0.0, 0.0, 1.0, 0.0);

	// Apply a rotation about the z-axis
	vec2 newVertPos;
	newVertPos.x = vertPos.x * cos(theta) - vertPos.y * sin(theta);
	newVertPos.y = vertPos.x * sin(theta) + vertPos.y * cos(theta);

	vec4 vertPosWorld = MV0 * vec4(radius * newVertPos.xy, 0.0, 1.0);
	gl_Position = P * vertPosWorld;
	fragTex = vertTex;

	// Calculate dist for fading.
	vec3 camDist = camPos - vertPosWorld.xyz;
	dist = length(vec2(camDist.x, camDist.z));
}
